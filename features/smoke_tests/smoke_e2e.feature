Feature: Registration flows

  @reg_thru_portal
  Scenario: Citizen Portal Flow.
    Given user is on Citizen portal HOME page
    When the user Register and books an appointment using the portal.

  @reg_user_thru_call_center
  Scenario: Call Center Agent Flow
    Given user is on call_center_agent Login Page.
    When the user provide the "call_center_agent" username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen.
    Then the user Register and books an appointment using the call_center_agent.

  @reg_user_thru_clinician
  Scenario: Clinician Flow.
    Given user is on clinician Login Page.
    When the user provide the clinician username and "Password", and clicks the "Login" button, the user is navigated to the "Home" screen.
    Then the user Register and books an appointment using the clinician.